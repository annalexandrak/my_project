
let images = [
    '2469.jpeg','tibetskiy-manul.jpg','6763045de3e8e691597b61c560345c57.jpg','scale_1200.webp', '2465.jpg'
]
let num = 0;
// нажимаем кнопу "Вперед"
function forwImg() {
    let slider = document.getElementById('slider');
    num++;
    // если предыдущий был последнем в массиве
    if (num >= images.length) {
        num = 0;
    }
    slider.src = images[num];
}
// нажимаем кнопу "Назад"
function backImg() {
    let slider = document.getElementById('slider');
    num--;
    // если предыдущий был первым в массиве
    if (num < 0) {
        num = images.length - 1;
    }
    slider.src = images[num];
}


let moveLeft = false;

function runAnimation() {

    setInterval(move, 30);
}

// получаем координаты элемента в контексте документа
function getCoords(elem) {
    let box = elem.getBoundingClientRect();

    return {  
        left: box.left + window.pageXOffset
    };
}


function move() {

    // получаем доступ к кругу
    let circle = document.getElementById('color')
    let container = document.getElementById('container');

    // вычисляем реальные координаты
    let cycleX = getCoords(circle).left;
    
    let containerX = getCoords(container).left;
   

    // будем использовать переменные чтобы
    // записывать текущее положение круга
    let posX = cycleX - containerX;
    

  
    if (posX >= 1070) moveLeft = true; // значит дошли до правой границы
    if (posX <= 0) moveLeft = false; // значит дошли до левой границы
    

    // меняем позицию в зависимости
    // от направления движения
    moveLeft ?
        // по горизонтали
        posX = cycleX - 3 : posX = cycleX + 3;
  
    color.style.left = posX+'px';

}
// когда загружаем страницу - запускаем анимацию
window.onload = () => {
    runAnimation();
}


function colorsFunc() {
    let colors = document.getElementById('colors');
    colors.style.backgroundColor = 'blue';
        }
